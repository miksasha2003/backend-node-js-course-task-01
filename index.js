const express = require('express')
const dayjs = require('dayjs')
const app = express()

app.use(express.json());
app.use(express.text());
app.use(express.urlencoded({ extended: true }));

// 1 - квадрат числа
app.post('/square', (req, res)=> {
    const number = Number(req.body);
    if (isNaN(number)) {
        return res.status(400).send("Було подано не число")
    }
    const result_sq = number * number;
    res.status(200).send({
        number: number,
        square: result_sq,
    });
});

// 2 - реверс тексту
app.post('/reverse', (req, res)=> {
    const str = req.body;
    const result_reverse = str.split("").reverse().join("");
    res.send(
        result_reverse
    );
});

// 3 - дані про дату
function isLeapYear(year) {
    return  (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
}

function checkCorrectDate(day, month, year) {
    const daysInMonth = {
        1: 31,
        2: isLeapYear(year) ? 29 : 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31,
    };
    return day <= daysInMonth[month];
}

app.get('/date/:year/:month/:day', (req, res)=> {
    const year = Number(req.params.year);
    const month = Number(req.params.month);
    const day = Number(req.params.day);

    if (!checkCorrectDate(day, month, year)) {
        return res.status(400).send("Було подано не правильні параметри");
    } else {
        const currentDate = dayjs().startOf('day');
        const date = new Date(req.params.year, --req.params.month, req.params.day);
        const days = ['Sunday', 'Monday', 'Tuesday',
            'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        const numberOfDays = Math.abs(Number(currentDate.diff(date, 'day')));
        res.send({
            'weekDay': days[date.getDay()],
            'isLeapYear': isLeapYear(req.params.year),
            'difference': numberOfDays,
        });
    }
});

const port = process.env.PORT || 56201;
app.listen(port, () => {
    console.log('App running on port ' + port);
})
